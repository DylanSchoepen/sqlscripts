USE ModernWays;
CREATE TABLE Nummers(
Titel VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Artiest VARCHAR (100) CHAR SET utf8mb4 NOT NULL,
Genre VARCHAR (50),
Jaar CHAR (4)
);

CREATE TABLE Huisdieren(
Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Leeftijd SMALLINT NOT NULL,
Soort VARCHAR (50) DEFAULT NULL
);

RENAME TABLE Nummers TO Liedjes;

ALTER TABLE Liedjes 
ADD ALBUM VARCHAR (100) CHAR SET utf8mb4 DEFAULT NULL;

ALTER TABLE Liedjes
DROP COLUMN Genre;

ALTER TABLE Huisdieren
ADD Baasje VARCHAR (100) CHAR SET utf8mb4 NOT NULL;

CREATE TABLE Metingen(
Tijdstip DATETIME NOT NULL,
Grootte SMALLINT NOT NULL,
CHECK (Grootte>0),
Marge DECIMAL (3,2) NOT NULL
);

INSERT INTO Liedjes(Titel, Artiest, Jaar, Album)
VALUES
		("John the Relevator", "Larkin Poe", "2017", "Peach"),
		("Missionary", "Ghost","2016" , "Popestar");
        INSERT INTO Huisdieren (Baasje, Naam, Leeftijd, Soort)
VALUES
		("Vincent", "Misty", 6, "Hond"),
        ("Christiane", "Ming", 8, "Hond"),
		("Esther", "Bientje", 6, "Kat"),
        ("Jommeke", "Flip", 75, "Papegaai"),
        ("Villads", "Berto", 1, "Papegaai"),
        ("Bert", "Ming", 7, "Kat"),
        ("Thaïs", "Suerta",2,"Hond"),
        ("Lyssa", "Фёдор", 1, "Hond");

SELECT * FROM Liedjes;

SELECT Naam, Soort FROM Huisdieren;

SELECT Titel FROM Liedjes;

SELECT * FROM Huisdieren ORDER by Naam, Soort;

SELECT Concat(Naam,' de ', Soort) AS Omschrijving FROM Huisdieren;

		

        
